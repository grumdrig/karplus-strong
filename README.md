Karplus-Strong
==============

Web audio API implementation of the Karplus-Strong plucked string
algorithm.

That is to say, this stuff is supposed to sound like a guitar.


References
----------

- <http://en.wikipedia.org/wiki/Karplus%E2%80%93Strong_string_synthesis>

- <http://users.soe.ucsc.edu/~karplus/papers/digitar.pdf>

- <http://www.jaffe.com/Jaffe-Smith-Extensions-CMJ-1983.pdf>

- <https://ccrma.stanford.edu/~jos/pasp/Extended_Karplus_Strong_Algorithm.html>

- Interesting-looking 3-band EQ: <http://robb.is/6strings/> and
  especially <http://robb.is/6strings/js/application.js>
